<?php
  /*
   * The code below displays a form with where users can submit song
   * requests. Note that requests are NOT handled internally, they are
   * simply sent via E-mail to the E-mail address configured for your
   * stream. Your E-mail address will not be disclosed to the public.
   */
?>
<div id="cc_request_result"></div>
<form>
<?php echo t('Song (artist/title)'); ?>:
<input type="text" id="cc_request_song" name="request[song]" size="40" maxlength="127" /><br />
<?php echo t('Dedicated to'); ?>:
<input type="text" id="cc_request_dedi" name="request[dedication]" size="40" maxlength="127" /><br />
<?php echo t('Your name');?>:
<input type="text" id="cc_request_sender" name="request[sender]" size="40" maxlength="127" /><br />
<?php echo t('Your E-mail');?>:
<input type="text" id="cc_request_email" name="request[email]" size="40" maxlength="127" /><br />
<input type="button" id="cc_request_button" value="<?php echo t('Submit song request'); ?>" onclick="cc_request_submit()" />
</form>