<?php
  /*
   * This snippet is similar to the Stream Status Summary, but it allows
   * you to control the exact placement of various information about
   * your stream through the use of individual <span> tags.
   */
?>
<?php echo t('Current song'); ?>:
<a href='<?php echo $current_song; ?>' id='cc_stream_info_song'><?php echo t('Loading...'); ?></a><br/>
<?php echo t('Stream title'); ?>:
<span id='cc_stream_info_title'></span><br/>
<?php echo t('Bit rate:'); ?>
<span id='cc_stream_info_bitrate'></span><br/>
<?php echo t('Current listeners'); ?>:
<span id='cc_stream_info_listeners'></span><br/>
<?php echo t('Maximum listeners'); ?>:
<span id='cc_stream_info_maxlisteners'></span><br/>
<?php echo t('Server status'); ?>:
<span id='cc_stream_info_server'></span><br/>
<?php echo t('Source status'); ?>:
<span id='cc_stream_info_source'></span>