<?php
  /*
   * This snippet will display a list of recently played tracks
   * (including Amazon affiliate links and album cover images if Amazon
   * integration is enabled).
   */
?>
<link rel="stylesheet" type="text/css" href="http://<?php echo $stream_ip; ?>/theme/widget_recenttracks.css" />
<div id="cc_recent_tracks"><?php echo t('Loading...'); ?></div>