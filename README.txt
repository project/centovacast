Copying the description of the Centova Cast from the website
http://www.centova.com/pages/cast
"Centova Cast Streaming Radio Control Panel is the world's most advanced
Internet radio stream hosting control panel. Manage a single station with ease,
or automate a stream hosting business with thousands of clients.  Centova Cast
can handle virtually any stream hosting scenario!"

Centova has many features! One among those is a list of 5 html+javascript
snippets to shown information about your radio station on your webpage.
Implementation of these code snippets in your webpage is not really much of a
difficulty as proper descriptions to help you exist. In Drupal though this
procedure is better implemented with a module. Centovacast module!

After installation, go to "admin/settings/centovacast" and fill-in your
CentovaCast username and your CentovaCast server IP. All you have to do then is
to go to "admin/build/block" and set the CentovaCast blocks you want to the
proper regions. That's all!