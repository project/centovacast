<?php
  /*
   * This snippet will display a browser for your on-demand content,
   * and allow your visitors to listen to and create playlists for your
   * content.
   */
?>
<link rel="stylesheet" type="text/css" href="http://<?php echo $stream_ip; ?>/theme/widget_ondemand.css" />
<div id="cc_on_demand_content"><?php t('Loading...'); ?></div>