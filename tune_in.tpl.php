<?php
  /*
   * This snippet will provide a set of tune-in links for various
   * media players.
   */
  $url = "http://{$stream_ip}/tunein.php/{$stream_username}";
  $image_url = "http://{$stream_ip}/system/images";
?>
<div id="cc_tunein">
  <a href="<?php echo $url;?>/playlist.pls">
    <img align="absmiddle" src="<?php echo $image_url; ?>/tunein-pls.png" alt="Winamp" title="Winamp" />
  </a>
  <a href="<?php echo $url; ?>/playlist.asx">
    <img align="absmiddle" src="<?php echo $image_url; ?>/tunein-asx.png" alt="windows Media Player" title="Windows Media Player" />
  </a>
  <a href="<?php echo $url; ?>/alignplaylist.ram">
    <img align="absmiddle" src="<?php echo $image_url; ?>/tunein-ram.png" alt="Real Player" title="Real Player" />
  </a>
  <a href="<?php echo $url; ?>/playlist.qtl">
    <img align="absmiddle" src="<?php echo $image_url; ?>/tunein-qtl.png" alt="QuickTime" title="QuickTime" />
  </a>
</div>
